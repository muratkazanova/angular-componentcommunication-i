import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {
  @Output() NewEmployee: EventEmitter<{
    FirstName: string;
    LastName: string;
  }> = new EventEmitter();
  @Output() Cancel: EventEmitter<void> = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  onNewEmployee(FirstName: string, LastName: string) {
    this.NewEmployee.emit({ FirstName, LastName });
  }

  onCancel() {
    this.Cancel.emit();
  }
}
