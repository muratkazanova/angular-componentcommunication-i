import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-employee-taskbar',
  templateUrl: './employee-taskbar.component.html',
  styleUrls: ['./employee-taskbar.component.scss']
})
export class EmployeeTaskbarComponent implements OnInit {
  @Input()
  employees: any;
  @Output() ShowEmployeeForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  constructor() {}

  ngOnInit() {}
  onNewEmployee() {
    this.ShowEmployeeForm.emit(true);
  }
}
