import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter
} from '@angular/core';
import { IEmployee } from 'src/app/models/employee';

@Component({
  selector: 'app-employee-item',
  templateUrl: './employee-item.component.html',
  styleUrls: ['./employee-item.component.scss']
})
export class EmployeeItemComponent implements OnInit {
  @Input()
  employee: IEmployee;
  isEdit = false;
  @ViewChild('edtFirstName')
  inputFirstName: ElementRef;
  @ViewChild('edtLastName')
  inputLastName: ElementRef;
  @Output() EmployeeUpdate: EventEmitter<IEmployee> = new EventEmitter();
  @Output() EmployeeDelete: EventEmitter<number> = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  onEdit() {
    this.isEdit = true;
  }
  onEmployeeUpdate(FirstName, LastName, Id) {
    const emp = Object.assign({}, this.employee, {
      FirstName: FirstName,
      LastName: LastName,
      id: Id
    });
    this.EmployeeUpdate.emit(emp);
    this.isEdit = false;
  }

  onEmployeeDelete(id: number) {
    this.EmployeeDelete.emit(id);
  }

  resetTemplate() {
    this.isEdit = false;
    this.inputFirstName.nativeElement.value = this.employee.FirstName;
    this.inputLastName.nativeElement.value = this.employee.LastName;
  }
}
