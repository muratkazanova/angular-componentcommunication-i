import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IEmployee } from 'src/app/models/employee';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  @Input()
  employees: IEmployee[];
  @Output()
  EmployeeDelete: EventEmitter<number> = new EventEmitter();
  @Output()
  EmployeeUpdate: EventEmitter<IEmployee> = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  onEmployeeUpdate(employee: IEmployee) {
    this.EmployeeUpdate.emit(employee);
  }

  onEmployeeDelete(id: number) {
    console.log(id);
    this.EmployeeDelete.emit(id);
  }
}
