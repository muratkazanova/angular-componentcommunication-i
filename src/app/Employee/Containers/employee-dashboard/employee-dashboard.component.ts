import { Component, OnInit } from '@angular/core';
import { IEmployee } from 'src/app/models/employee';

@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.scss']
})
export class EmployeeDashboardComponent implements OnInit {
  employees = [];
  isNewEmployee = false;
  constructor() {}

  ngOnInit() {
    this.getEmployees();
  }

  onNewEmployee(value: { FirstName: string; LastName: string }) {
    const ids = this.employees.map(o => o['id']).sort();

    this.employees = [
      ...this.employees,
      {
        id: ids.length ? ids[ids.length - 1] + 1 : 1,
        FirstName: value.FirstName,
        LastName: value.LastName
      }
    ];
    this.isNewEmployee = false;
  }

  onEmployeeDelete(id: number) {
    this.employees = this.employees.filter(e => e.id != id);
  }

  onEmployeeUpdate(employee: IEmployee) {
    const i = this.employees.findIndex(e => e.id == employee.id);
    this.employees[i] = employee;
  }

  onShowEmployeeForm(value: boolean) {
    this.isNewEmployee = value;
  }

  onEmployeeFormCancel() {
    this.isNewEmployee = false;
  }
  getEmployees() {
    this.employees = [
      {
        id: 1,
        FirstName: 'Ken',
        LastName: 'Sanch',
        Gender: 'M',
        EmailAddress: 'ken0@adventure-works.com',
        PhoneNumber: '697-555-0142'
      },
      {
        id: 2,
        FirstName: 'Terri',
        LastName: 'Duffy',
        Gender: 'F',
        EmailAddress: 'terri0@adventure-works.com',
        PhoneNumber: '819-555-0175'
      },
      {
        id: 3,
        FirstName: 'Roberto',
        LastName: 'Tamburell',
        Gender: 'M',
        EmailAddress: 'roberto0@adventure-works.com',
        PhoneNumber: '212-555-0187'
      }
    ];
  }
}
